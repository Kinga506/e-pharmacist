const express = require('express')
const app = express()
const userRouter = require('./routes/userRoutes')
/* Starting the server on port 4001. */
// const port = 4001
// app.listen(port, () => {
//     console.log(`App running on port ${port} ..`)
// })

module.exports = app

app.use(express.json())
app.use('/api/v1/users', userRouter)